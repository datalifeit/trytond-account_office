=======================
Account Office Scenario
=======================

Imports::

    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts


Install office_product::

    >>> config = activate_modules(['account_office', 'office_product', 'sale_office'])


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Get user::

    >>> User = Model.get('res.user')
    >>> admin = User(config.user)


Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']
    >>> receivable = accounts['receivable']


Create party::

    >>> Party = Model.get('party.party')
    >>> party = Party(name='Party')
    >>> party.save()


Create offices::

    >>> Office = Model.get('company.office')
    >>> office1 = Office()
    >>> office1.name = 'Office 1'
    >>> office1.company = company
    >>> office1.save()
    >>> office2 = Office()
    >>> office2.name = 'Office 2'
    >>> office2.company = company
    >>> office2.save()


Add offices to admin user and reload context::

    >>> admin.offices.append(office1)
    >>> admin.offices.append(office2)
    >>> admin.save()
    >>> config._context = User.get_preferences(True, config.context)
    >>> office1 = Office(office1.id)
    >>> office2 = Office(office2.id)


Create Product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> template = ProductTemplate()
    >>> template.name = 'product'
    >>> template.default_uom = unit
    >>> template.type = 'service'
    >>> template.list_price = Decimal('40')
    >>> template.offices.append(office1)
    >>> template.save()
    >>> product, = template.products


Create Invoice::

    >>> Journal = Model.get('account.journal')
    >>> journal_revenue, = Journal.find([
    ...         ('code', '=', 'REV'),
    ...         ])
    >>> office2 = Office(office2.id)
    >>> journal_revenue.offices.append(office2)
    >>> journal_revenue.save()
    >>> Invoice = Model.get('account.invoice')
    >>> invoice = Invoice()
    >>> invoice.party = party
    >>> invoice.office != None
    True
    >>> line = invoice.lines.new()
    >>> line.product = product
    >>> line.quantity = 2.0
    >>> line.unit_price = Decimal('40')
    >>> line.account = revenue
    >>> invoice.save()
    Traceback (most recent call last):
    ...
    trytond.model.modelstorage.DomainValidationError: The value for field "Product" in "Invoice Line" is not valid according to its domain. - 

    >>> office2 = Office(office2.id)
    >>> template.offices.append(office2)
    >>> template.save()
    >>> invoice.save()


Check product remove constraint::

    >>> office1 = Office(office1.id)
    >>> office2 = Office(office2.id)
    >>> index = template.offices.index(office2)
    >>> _ = template.offices.pop(index)
    >>> template.save() # doctest: +ELLIPSIS
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Cannot delete office "Office 2" for Product Template "product" due to it is used on document "Invoice Line" "...". - 
    >>> template.reload()
    >>> template.offices.append(office1)
    >>> template.save()

Check line office domain for origin (using sales)::

    >>> InvoiceLine = Model.get('account.invoice.line')
    >>> Sale = Model.get('sale.sale')
    >>> template.reload()
    >>> template.salable = True
    >>> template.save()
    >>> product.reload()
    >>> sale = Sale()
    >>> sale.party = party
    >>> sale.office = office1
    >>> sline = sale.lines.new()
    >>> sline.product = product
    >>> sline.quantity = 2.0
    >>> sale.save()
    >>> line2 = InvoiceLine()
    >>> line2.invoice_type = 'out'
    >>> line2.party = party
    >>> line2.account = revenue
    >>> line2.quantity = 1
    >>> line2.unit_price = Decimal(10)
    >>> line2.office = office2
    >>> line2.origin = sale.lines[0]
    >>> line2.origin_office == sale.lines[0].office == office1
    True
    >>> line2.save()
    Traceback (most recent call last):
        ...
    trytond.model.modelstorage.DomainValidationError: The value for field "Office" in "Invoice Line" is not valid according to its domain. - 

    >>> line2.office = office1
    >>> line2.save()
    >>> line2.origin_office == sale.lines[0].office == office1
    True
