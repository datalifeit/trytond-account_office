===============================
Office Account Product Scenario
===============================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import \
    ...     create_chart, get_accounts
    >>> from decimal import Decimal


Install office_account_product::

    >>> config = activate_modules(['account_office', 'office_account_product'])


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Get user::

    >>> User = Model.get('res.user')
    >>> admin = User(config.user)


Create offices::

    >>> Office = Model.get('company.office')
    >>> office1 = Office()
    >>> office1.name = 'Office 1'
    >>> office1.company = company
    >>> office1.save()
    >>> office2 = Office()
    >>> office2.name = 'Office 2'
    >>> office2.company = company
    >>> office2.save()


Add office to admin user and reload context::

    >>> admin.offices.append(office1)
    >>> office1 = Office(office1.id)
    >>> admin.offices.append(office2)
    >>> office2 = Office(office2.id)
    >>> admin.office = office1
    >>> admin.save()
    >>> config._context = User.get_preferences(True, config.context)


Create accounts::

    >>> Account = Model.get('account.account')
    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']
    >>> payable = accounts['payable']
    >>> revenue2, = revenue.duplicate()
    >>> revenue2.name = 'Revenue 2'
    >>> revenue2.save()
    >>> expense2, = expense.duplicate()
    >>> expense2.name = 'Expense 2'
    >>> expense2.save()


Create account category::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name='Account Category')
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.save()
    >>> admin.office = office2
    >>> admin.save()
    >>> config._context = User.get_preferences(True, config.context)
    >>> account_category = ProductCategory(account_category.id)
    >>> not account_category.account_expense
    True
    >>> account_category.account_expense = expense2
    >>> not account_category.account_revenue
    True
    >>> account_category.account_revenue = revenue2
    >>> account_category.save()
    >>> admin.office = office1
    >>> admin.save()
    >>> config._context = User.get_preferences(True, config.context)


Create party::

    >>> Party = Model.get('party.party')
    >>> party = Party(name='Party')
    >>> party.account_payable = payable
    >>> party.save()


Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.default_uom = unit
    >>> template.type = 'service'
    >>> template.list_price = Decimal('50')
    >>> template.account_category = account_category
    >>> template.save()
    >>> product, = template.products
    >>> product.cost_price = Decimal('20')
    >>> product.save()


Add office to journal::

    >>> Journal = Model.get('account.journal')
    >>> journal_expense, = Journal.find([('code', '=', 'EXP')])
    >>> journal_expense.offices.append(office1)
    >>> office1 = Office(office1.id)
    >>> journal_expense.offices.append(office2)
    >>> office2 = Office(office2.id)
    >>> journal_expense.save()


Create invoice::

    >>> Invoice = Model.get('account.invoice')
    >>> invoice = Invoice()
    >>> invoice.type = 'in'
    >>> invoice.party = party
    >>> invoice.office == office1
    True
    >>> line = invoice.lines.new()
    >>> line.product = product
    >>> line.account == expense
    True
    >>> while invoice.lines:
    ...     _ = invoice.lines.pop()
    >>> invoice.office = office2
    >>> line = invoice.lines.new()
    >>> line.product = product
    >>> line.account == expense2
    True
    >>> line.quantity = 1
    >>> line.unit_price = Decimal(10)
    >>> invoice.save()


Create invoice line

    >>> InvoiceLine = Model.get('account.invoice.line')
    >>> invoice_line = InvoiceLine()
    >>> invoice_line.invoice_type = 'out'
    >>> invoice_line.party = party
    >>> invoice_line.office = office1
    >>> invoice_line.product = product
    >>> invoice_line.account == revenue
    True
    >>> invoice_line.product = None
    >>> invoice_line.office = office2
    >>> invoice_line.product = product
    >>> invoice_line.account == revenue2
    True
    >>> invoice_line.quantity = 1
    >>> invoice_line.unit_price = Decimal(10)
    >>> invoice_line.save()
