# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, Workflow, fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, If, Bool
from trytond.transaction import Transaction
from trytond.i18n import gettext
from trytond.exceptions import UserError
from trytond.modules.company_office.office import OfficeMixin


class Journal(metaclass=PoolMeta):
    __name__ = 'account.journal'

    offices = fields.Many2Many('account.journal.office', 'journal', 'office',
        'Offices', states={'readonly': ~Eval('active')},
        depends=['active'])


class JournalOffice(ModelSQL):
    """Journal Office"""
    __name__ = 'account.journal.office'

    company = fields.Many2One('company.company', 'Company')
    journal = fields.Many2One(
        'account.journal', "Journal", ondelete='CASCADE', select=True)
    office = fields.Many2One('company.office', 'Office',
        domain=[
            ('company', 'in', [Eval('company', -1), None])
        ],
        depends=['company'])

    @staticmethod
    def default_company():
        return Transaction().context.get('company', None)


class Invoice(OfficeMixin, metaclass=PoolMeta):
    __name__ = 'account.invoice'

    allowed_offices = fields.Function(
        fields.Many2Many('company.office', None, None, 'Allowed offices'),
        'on_change_with_allowed_offices')

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.office.domain = [If(Eval('state') == 'draft', [
            ('id', 'in', Eval('allowed_offices', [])),
            ['OR',
                ('company', '=', None),
                ('company', '=', Eval('company'))
            ]], [])
        ]
        cls.office.depends.append('allowed_offices')
        add_remove = ['OR',
            ('office', '=', None),
            ('office', 'in', If(Bool(Eval('office')),
                [Eval('office'), ],
                Eval('allowed_offices', []))
            )
        ]
        cls.journal.domain.append(
            If(Eval('state') == 'draft',
                ('offices', 'in', Eval('context', {}).get('offices', [])),
                ()
            )
        )

        if not cls.lines.add_remove:
            cls.lines.add_remove = [add_remove]
        else:
            cls.lines.add_remove.append(add_remove)
        cls.lines.depends.extend(['office', 'allowed_offices'])

    @fields.depends('journal', 'type', methods=['on_change_journal',
        'on_change_with_allowed_offices'])
    def on_change_type(self):
        pool = Pool()
        Journal = pool.get('account.journal')

        journal_type = {
            'out': 'revenue',
            'in': 'expense',
            }.get(self.type or 'out', 'revenue')
        journals = Journal.search([
            ('type', '=', journal_type),
            ('offices', '=', Transaction().context.get('office', None)),
            ], limit=1)
        if journals:
            self.journal, = journals
        else:
            super().on_change_type()
        if self.journal:
            self.allowed_offices = self.on_change_with_allowed_offices()
            self.on_change_journal()

    @fields.depends('journal', '_parent_journal.offices')
    def on_change_journal(self):
        if self.journal and self.journal.offices and \
                len(self.journal.offices) == 1:
            self.office = self.journal.offices[0]

    @fields.depends('journal', '_parent_journal.offices')
    def on_change_with_allowed_offices(self, name=None):
        res = []
        if self.journal and self.journal.offices:
            res.extend([o.id for o in self.journal.offices])
            res = list(set(res) & set(
                Transaction().context.get('offices', [])))
        return res

    def _credit(self, **values):
        credit = super()._credit(**values)
        credit.office = self.office
        return credit

    @classmethod
    def validate(cls, records):
        super().validate(records)
        for record in records:
            record.check_office()

    @classmethod
    @ModelView.button
    @Workflow.transition('validated')
    def validate_invoice(cls, invoices):
        for invoice in invoices:
            invoice.check_office()
        super().validate_invoice(invoices)

    def check_office(self):
        if self.state != 'draft':
            return
        offices = self.allowed_offices
        if self.office:
            offices = [self.office]
        for line in self.lines:
            if not line.office:
                continue
            if line.office not in offices:
                raise UserError(gettext(
                    'account_office.msg_invoice_wrong_office',
                    invoice_line=line.rec_name,
                    office=line.office.rec_name,
                    offices='\n'.join(o.rec_name for o in offices)))


class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    office = fields.Many2One('company.office', 'Office',
        domain=[If(Eval('origin_office'),
            ('id', '=', Eval('origin_office')), ())],
        states={
            'readonly': ((Eval('invoice_state') != 'draft') |
                Bool(Eval('origin')))
        }, depends=['invoice_state', 'origin', 'origin_office'])

    origin_office = fields.Function(
        fields.Many2One('company.office', 'Origin office'),
        'on_change_with_origin_office')

    @fields.depends('invoice', 'origin', '_parent_invoice.office')
    def on_change_invoice(self):
        try:
            super().on_change_invoice()
        except AttributeError:
            pass
        if self.invoice and not self.origin:
            self.office = self.invoice.office

    @fields.depends('origin')
    def on_change_with_origin_office(self, name=None):
        if self.origin and getattr(self.origin, 'office', None):
            return self.origin.office.id
        return None

    def _credit(self):
        line = super()._credit()
        line.office = self.office
        return line


class InvoiceLine2(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    @fields.depends('invoice', '_parent_invoice.office', 'office')
    def on_change_product(self):
        if self.invoice and self.invoice.office:
            office = self.invoice.office.id
        elif self.office:
            office = self.office.id
        else:
            office = Transaction().context.get('office')
        with Transaction().set_context(office=office):
            super().on_change_product()


class InvoiceLine3(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.product.domain.append(If(Bool(Eval('office')),
            ['OR',
                ('offices', '=', None),
                ('offices', '=', Eval('office'))
            ],
            []))
        cls.product.depends.append('office')


class InvoiceStandAloneLine(metaclass=PoolMeta):
    __name__ = 'account.invoice_standalone_line'

    @classmethod
    def _standalone_line_group_key(cls, lines, line):
        res = super()._standalone_line_group_key(lines, line)
        return res + (('office', line.office), )
