# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta

__all__ = ['TemplateOffice']


class TemplateOffice(metaclass=PoolMeta):
    __name__ = 'product.template-company.office'

    @classmethod
    def _get_models_to_check(cls):
        return super()._get_models_to_check() + [
            ('account.invoice.line', 'office', 'product.template'),
            ('account.invoice.line', 'invoice.office', 'product.template'),
        ]
